#!/bin/bash

source `dirname "$0"`/config.sh || exit 1;

REPO_DIR=`git rev-parse --show-toplevel`

## Figure out which commits we're about to push

# Learn which branch we're on
LOCAL_BRANCH="$(git symbolic-ref HEAD 2>/dev/null)" ||
LOCAL_BRANCH="no branch"

LOCAL_BRANCH=${LOCAL_BRANCH##refs/heads/}

# This gets overwritten by the first argument, if it was provided
if [ $# -gt 0 ]; then
  LOCAL_BRANCH="$1";
  git rev-parse refs/heads/$LOCAL_BRANCH 2>/dev/null ||
  (echo "First argument isn't a local branch" && exit 1);
fi

# Make sure we have a local branch
if [ "$LOCAL_BRANCH" = "no branch" ]; then
  echo "Not on any branch, and no branch name passed."
  exit 1;
fi

# Set up the remote branch. Any name is fine with us
REMOTE_BRANCH=$LOCAL_BRANCH

if [ $# -gt 1 ]; then
  REMOTE_BRANCH="$2";
fi

# Update origin and our repo
git fetch origin;
git fetch $PUSH_TO_REMOTE;

# Find all the commits we don't have in $PUSH_TO_REMOTE nor origin/master.
# We need to test those. Testing the others is redundant and would only slow
# us down / lead to sadness if origin/master doesn't compile.

# remote branches (this might be easier with for-each-ref or so, but I didn't
# figure out how to make it work with remotes). ls-remote also works, but is
# slower - and we fetched the remote already anyway.
BRANCHES=`git branch -r | grep "^  $PUSH_TO_REMOTE"`

# List all commits only reachable from $LOCAL_BRANCH
COMMITS=`git rev-list $LOCAL_BRANCH --not origin/master $BRANCHES`

# Bail out if any commit is a merge commit. Maybe eventually we can handle this
# situation, but then we can't use the autosquash thing below probably.
# XXX Investigate.
NO_MERGE=`git rev-list --no-merges $LOCAL_BRANCH --not origin/master $BRANCHES`

if [ "$COMMITS" != "$NO_MERGE" ]; then
  echo "We don't support merge commits in the history";
  exit 1;
fi

# Find the "oldest" commit (first in history). We can easily abuse
# merge-base here, because all commits are linear. When we have it, use its
# ancestor as the last commit we *don't* have to check. XXX There must be a better
# way to do this.
BASE="`git merge-base --octopus $COMMITS`^"

## Get ready to test them. We want to autosquash, so that fixup commits
## don't break us.

# We don't want to type :wq for the autosquash
export GIT_EDITOR=true

# Create the tmp dir and cd into it. If it contained anything before, remove
# that stuff.
mkdir -p "$GIT_TMP_DIR";
cd "$GIT_TMP_DIR";
rm -rf repo;

# Clone the repository over. This should be quick, and we use it as a
# safeguard so we don't mess up the real repository.
git clone "$REPO_DIR" repo;
cd repo;
git checkout "$LOCAL_BRANCH";

# Do autosquash rebase against the last commit that is online, to fold in
# fixup commits. Without that we'd have to amend all the commits by hand
# XXX This doesn't work with merges. Probably nothing we can do about it.
# Not that I care too much, since I'm concerned with feature branches which
# shouldn't have merges in them anyway.
git rebase -i --autosquash $BASE;

## Make sure all commits pass all our tests. If one doesn't, whine.

# Learn the commit ids

COMMIT_IDS=`git log --reverse --pretty=format:%H $BASE..HEAD`

for id in $COMMIT_IDS; do
  git checkout $id;
  git clean -fqdx;
  ./autogen.sh;
  ./configure $CONFIGURE_ARGS;
  make $MAKE_ARGS;
  make test;
  SPACES=`make --quiet check-spaces`;
  if [ "$SPACES" != "" ]; then
    make check-spaces;
    exit 1;
  fi
  # Disabled because it takes forever.
  #make distcheck $MAKE_ARGS;
done

## Actually push stuff
cd $REPO_DIR;

# Don't allow force-push for now. Too dangerous
git push $PUSH_TO_REMOTE $LOCAL_BRANCH:$REMOTE_BRANCH
